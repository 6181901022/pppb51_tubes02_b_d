package com.example.travel;

public class InputPesan {
    String kota1;
    String kota2;
    String tanggal;
    String jam;
    public InputPesan(String kota1,String kota2,String tanggal, String jam){
        this.kota1=kota1;
        this.kota2=kota2;
        this.tanggal=tanggal;
        this.jam=jam;
    }

    public String getKota1() {
        return kota1;
    }

    public String getKota2() {
        return kota2;
    }

    public String getTanggal() {
        return tanggal;
    }

    public String getJam() {
        return jam;
    }

    public void setKota1(String kota1) {
        this.kota1 = kota1;
    }

    public void setKota2(String kota2) {
        this.kota2 = kota2;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public void setJam(String jam) {
        this.jam = jam;
    }
}
