package com.example.travel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.travel.databinding.FragmentLoginBinding;

public class FragmentLogin extends Fragment implements View.OnClickListener{
    //ActivityMainBinding binding;
    FragmentLoginBinding binding;
    LoginPresenter login;
    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = FragmentLoginBinding.inflate(inflater, container, false);
        this.binding.btnLogin.setOnClickListener(this);
        View view = this.binding.getRoot();

        this.login = new LoginPresenter(this.getActivity());

        return view;

    }

    @Override
    public void onClick(View view) {
        if(view==this.binding.btnLogin){
            Bundle result = new Bundle();
            result.putInt("page", 2);
            String username= this.binding.etUsername.getText().toString();
            String password=this.binding.etPassword.getText().toString();
            login.execute(username,password);
            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }

}
