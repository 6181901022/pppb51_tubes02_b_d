package com.example.travel;

public class InputNewOrder {
    String course_id;
    String seat;
    public InputNewOrder(String course_id,String seat){
        this.course_id=course_id;
        this.seat=seat;
    }

    public String getCourse_id() {
        return course_id;
    }

    public String getSeat() {
        return seat;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public void setSeat(String seat) {
        this.seat = seat;
    }
}
