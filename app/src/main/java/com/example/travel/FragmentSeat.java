package com.example.travel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.travel.databinding.FragmentSeatBinding;

public class FragmentSeat extends Fragment implements View.OnClickListener {
    FragmentSeatBinding binding;
    SeatPresenter seat;
    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = FragmentSeatBinding.inflate(inflater, container, false);
        this.binding.btnLanjut.setOnClickListener(this);
        View view = this.binding.getRoot();

        this.seat = new SeatPresenter(this.getActivity());
        
        return view;

    }

    @Override
    public void onClick(View view) {
        if(view==this.binding.btnLanjut){
            Bundle result = new Bundle();
            result.putInt("page", 5);

            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }
}
