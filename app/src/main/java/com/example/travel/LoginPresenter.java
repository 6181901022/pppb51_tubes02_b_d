package com.example.travel;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginPresenter {
    private Context context;
    private Gson gson;
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/authenticate";
    private JSONObject json;

    public LoginPresenter(Context context) {
        this.context = context;
    }
    public void execute(String username,String password){
        InputLogin input=new InputLogin("6181901022", "220109816");
        this.gson=new Gson();
        try {
            json=new JSONObject(gson.toJson(input));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        callVolley(json);
    }
    private void callVolley(JSONObject toJson) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, toJson,
                response ->  {
                        try {
                            process(response.toString());
                            Toast.makeText(context, "Login Success!",
                                    Toast.LENGTH_SHORT).show();
                            Log.d("RESPONSE-TOKEN",response.getString("token"));
                        } catch (JSONException e) {
                            Log.d("RESPONSE-ERROR","error");
                            e.printStackTrace();
                        }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "Login gagal",
                        Toast.LENGTH_SHORT).show();
                Log.d("RESPONSE-ERROR","error");
            }
        });
        requestQueue.add(jsonRequest);
    }
    private void process(String json) {
        ResponLogin respon= this.gson.fromJson(json,ResponLogin.class);
        String token = respon.getToken();
        String pesan = respon.getPesan();

    }


}
