package com.example.travel;

public class ResponLogin {
    String token;
    String pesan;
    public ResponLogin(String token,String pesan){
        this.token=token;
        this.pesan=pesan;
    }

    public String getPesan() {
        return pesan;
    }

    public String getToken() {
        return token;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
