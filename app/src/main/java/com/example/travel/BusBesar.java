package com.example.travel;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.travel.databinding.FragmentSeatBinding;

public class BusBesar extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, GestureDetector.OnGestureListener, ScaleGestureDetector.OnScaleGestureListener {
    private Bitmap mBitmap;
    private FragmentSeatBinding  binding;
    private Canvas mCanvas;
    private Paint kosong,isi,numSeat,numSeatIsi;
    private ImageView ivCanvas;
    private GestureDetector mDetector;
    private int [] busBesar;
    InitCanvas initCanvas;
    public  String dipilih;//kursi yang dipilih

    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = FragmentSeatBinding.inflate(inflater, container, false);

        View view = this.binding.getRoot();
        this.initCanvas=new InitCanvas();
        initCanvas.bitMap();
        initCanvas.bg();
        initCanvas.pain();
        this.busBesar=new int[10];//inisiasi bus besar. jika 1 maka udah di klik dan 0 belum di klik
        for(int a=0;a<10;a++){
            busBesar[a]=0;
        }
        this.dipilih="";
        return view;

    }

    @Override
    protected void onResume() {
        super.onResume();
        initCanvas.gambarBesar();
    }

    @Override
    public boolean onDown(MotionEvent motionEvent) {
        float x=motionEvent.getX();
        float y=motionEvent.getY();
        if(x>=150 && x<=220 && y>=150 && y<=220){
            if(busBesar[0]==0){//seat 1 dipilih
                mCanvas.drawRect(150,150,220,220,isi);
                mCanvas.drawText("1",180,190,numSeat);
                busBesar[0]=1;
            }else{//sudah ditekan dan mau seat mau dikosongin
                mCanvas.drawRect(150,150,220,220,kosong);
                mCanvas.drawText("1",180,190,numSeat);
                busBesar[0]=0;
            }
        }
        if(x>=260 && x<=330 && 150<=x && x<=220){//seat 2 dipilih
            if(busBesar[1]==0){ //
                mCanvas.drawRect(260,150,330,220,isi);
                mCanvas.drawText("2",290,185,numSeat);
                busBesar[1]=1;
            }else{
                mCanvas.drawRect(260,150,330,220,kosong);
                mCanvas.drawText("2",290,185,numSeat);
            }
        }
        if(x>=10 && x<=80 && y>=250 && y<=320){//seat 3
            if(busBesar[2]==0){
                mCanvas.drawRect(10,250,80,320,isi);
                mCanvas.drawText("3",40,285,numSeat);
            }else{
                mCanvas.drawRect(10,250,80,320,kosong);
                mCanvas.drawText("3",40,285,numSeat);
            }
        }
        if(x>=260 && x<=330 && y>=250 && y<=320){//seat 4
            if(busBesar[3]==0){
                mCanvas.drawRect(260,250,330,320,isi);
                mCanvas.drawText("4",295,285,numSeat);
            }else{
                mCanvas.drawRect(260,250,330,320,kosong);
                mCanvas.drawText("4",295,285,numSeat);
            }
        }
        if(x>=10 && x<=80 && y>=350 && y<= 420){//seat 5
            if(busBesar[4]==0){
                mCanvas.drawRect(10,350,80,420,isi);
                mCanvas.drawText("5",45,385,numSeat);
            }else{
                mCanvas.drawRect(10,350,80,420,kosong);
                mCanvas.drawText("5",45,385,numSeat);
            }
        }
        if(x>=260 && x<=330 && y>=350 && y<= 420){//seat 6
            if(busBesar[5]==0){
                mCanvas.drawRect(260,350,330,420,isi);
                mCanvas.drawText("6",195,385,numSeat);
            }else{
                mCanvas.drawRect(260,350,330,420,kosong);
                mCanvas.drawText("6",195,385,numSeat);
            }
        }
        if(x>=150 && x<=220 && y>=350 && y<=420){//seat 7
            if(busBesar[6]==0){
                mCanvas.drawRect(150,350,220,420,isi);
                mCanvas.drawText("7",295,385,numSeat);
            }else{
                mCanvas.drawRect(150,350,220,420,kosong);
                mCanvas.drawText("7",295,385,numSeat);
            }
        }
        if(x>=10 && x<=80 && y>=450 && y<=520){//seat 8
            if(busBesar[7]==0){
                mCanvas.drawRect(10,450,80,520,isi);
                mCanvas.drawText("8",45,485,numSeat);
            }else{
                mCanvas.drawRect(10,450,80,520,kosong);
                mCanvas.drawText("8",45,485,numSeat);
            }
        }
        if(x>=150 && x<=220 && y>= 450 && y<=520){//seat 9
            if(busBesar[8]==0){
                mCanvas.drawRect(150,450,220,520,isi);
                mCanvas.drawText("9",185,485,numSeat);
            }else{
                mCanvas.drawRect(150,450,220,520,kosong);
                mCanvas.drawText("9",185,485,numSeat);
            }
        }
        if(x>=260 && x<=330 && y<=450 && y>=520){
            if(busBesar[9]==0){
                mCanvas.drawRect(260,450,330,520,isi);
                mCanvas.drawText("10",295,485,numSeat);
            }else{
                mCanvas.drawRect(260,450,330,520,kosong);
                mCanvas.drawText("10",295,485,numSeat);
            }
        }

        this.binding.ivSeat.invalidate();
        return false;
    }
    //mencatat seat yg dipilih
    public String pilih(){
        for(int a=0;a<10;a++){
            if(this.busBesar[a]==1){
                int seat=a+1;
                this.dipilih = this.dipilih + seat;
            }
            this.dipilih+=",";
        }
        return dipilih;
    }
    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}
