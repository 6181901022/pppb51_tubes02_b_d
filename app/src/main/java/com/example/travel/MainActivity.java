package com.example.travel;

import android.content.Context;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

public class MainActivity extends AppCompatActivity {
    //private int waktu_loading=4000;
    FragmentManager fm;
    FragmentLogin login;
    FragmentHome home;
    FragmentSeat seat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.login=new FragmentLogin();
        this.home=new FragmentHome();
        this.seat= new FragmentSeat();
        CalligraphyConfig.initDefault(new CalligraphyConfig().Builder()
                .setDefaultFontPath("Cormorant.ttf")
                .setFontAttrId(R.attr.fontPath)
        .build());
    }

    public void changePage(int page){
        FragmentTransaction ft=this.fm.beginTransaction();
        if(page==1){
            if(this.login.isAdded()){
                ft.show(this.login);
            }else{
                ft.add(R.id.container,this.login).addToBackStack(null);
            }
        }
        if(page==2){
            if(this.home.isAdded()){
                ft.show(this.home);
            }else{
                ft.add(R.id.container,this.home).addToBackStack(null);
            }
        }
        if(page==4){
            if(this.seat.isAdded()){
                ft.show(this.seat);
            }else{
                ft.add(R.id.container,this.seat).addToBackStack(null);
            }
        }
    }

}