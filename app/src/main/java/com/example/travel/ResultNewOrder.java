package com.example.travel;

import java.util.List;

public class ResultNewOrder {
    String pesan;
    List<Seat> seat;
    ResultNewOrder(String pesan,List <Seat> seat){
        this.pesan=pesan;
        this.seat=seat;
    }

    public String getPesan() {
        return pesan;
    }

    public List<Seat> getSeat() {
        return seat;
    }

    public void setSeat(List<Seat> seat) {
        this.seat = seat;
    }

    public void setPesan(String pesan) {
        this.pesan = pesan;
    }
}
