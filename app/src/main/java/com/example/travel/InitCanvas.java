package com.example.travel;
import android.graphics.Bitmap;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import com.example.travel.databinding.FragmentSeatBinding;

public class InitCanvas extends AppCompatActivity {
    private Bitmap mBitmap;
    private FragmentSeatBinding binding;
    private android.graphics.Canvas mCanvas;
    private Paint kosong,isi,numSeat,numSeatIsi;
    private ImageView ivCanvas;

    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = FragmentSeatBinding.inflate(inflater, container, false);
        View view =this.binding.getRoot();
        return view;

    }
    //create bitmap
    void bitMap(){
        this.mBitmap= Bitmap.createBitmap(500,600, Bitmap.Config.ARGB_8888);
        //asosiasi bitmap
        this.binding.ivSeat.setImageBitmap(mBitmap);
        //buat canvas
        this.mCanvas= new android.graphics.Canvas(mBitmap);

    }
    //create background
    void bg(){
        int mCB= ResourcesCompat.getColor(getResources(),R.color.white,null);
        mCanvas.drawColor(mCB);
    }
    //create pain
    void pain(){
        //seat kosong
        this.kosong = new Paint();
        int mColorTest= ResourcesCompat.getColor(getResources(),R.color.cream,null);
        this.kosong.setColor(mColorTest);
        this.kosong.setStyle(Paint.Style.FILL);
        this.numSeat= new Paint();
        numSeat.setColor(this.getResources().getColor(R.color.black));
        this.numSeat.setTextSize(18);
        //seat yg dipilih
        this.isi = new Paint();
        mColorTest= ResourcesCompat.getColor(getResources(),R.color.teal_200,null);
        this.isi.setColor(mColorTest);
        this.isi.setStyle(Paint.Style.FILL);
        this.numSeatIsi= new Paint();
        numSeatIsi.setColor(this.getResources().getColor(R.color.white));
        this.numSeatIsi.setTextSize(18);


    }

    //create seat
    void gambarBesar(){

        //seat supir
        mCanvas.drawRect(10,10,80,80,kosong);
        mCanvas.drawText("S",40,45,numSeat);
//        //seat2
        mCanvas.drawRect(150,150,220,220,kosong);
        mCanvas.drawText("1",180,190,numSeat);
//        //seat 3
        mCanvas.drawRect(260,150,330,220,kosong);
        mCanvas.drawText("2",290,185,numSeat);
////        //seat 4
        mCanvas.drawRect(10,250,80,320,kosong);
        mCanvas.drawText("3",40,285,numSeat);
        //Seat5
        mCanvas.drawRect(260,250,330,320,kosong);
        mCanvas.drawText("4",295,285,numSeat);

        //seat 6
        mCanvas.drawRect(10,350,80,420,kosong);
        mCanvas.drawText("5",45,385,numSeat);
        //Seat7
        mCanvas.drawRect(260,350,330,420,kosong);
        mCanvas.drawText("6",195,385,numSeat);
        //seat 8
        mCanvas.drawRect(150,350,220,420,kosong);
        mCanvas.drawText("7",295,385,numSeat);
        //seat 9
        mCanvas.drawRect(10,450,80,520,kosong);
        mCanvas.drawText("8",45,485,numSeat);
        //seat 10
        mCanvas.drawRect(150,450,220,520,kosong);
        mCanvas.drawText("9",185,485,numSeat);
        //Seat11
        mCanvas.drawRect(260,450,330,520,kosong);
        mCanvas.drawText("10",295,485,numSeat);
    }
    void gambarKecil(){
        //seat supir
        mCanvas.drawRect(10,10,80,80,kosong);
        mCanvas.drawText("s",40,45,numSeat);
        //seat1
        mCanvas.drawRect(190,10,260,80,kosong);
        mCanvas.drawText("1",225,45,numSeat);
        //seat2
        mCanvas.drawRect(10,110,80,180,kosong);
        mCanvas.drawText("2",40,145,numSeat);
//        //seat3
//        mCanvas.drawRect(100,110,170,180,kosong);
//        mCanvas.drawText("3",135,145,numSeat);
        //seat3
        mCanvas.drawRect(190,110,260,180,kosong);
        mCanvas.drawText("3",225,145,numSeat);
        //seat4
        mCanvas.drawRect(10,210,80,280,kosong);
        mCanvas.drawText("4",40,245,numSeat);
        //seat5
        mCanvas.drawRect(100,210,170,280,kosong);
        mCanvas.drawText("5",135,245,numSeat);
        //seat6
        mCanvas.drawRect(190,210,260,280,kosong);
        mCanvas.drawText("6",225,245,numSeat);
    }
}
