package com.example.travel;

import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.os.Bundle;
import android.widget.Toast;
import android.util.Log;
import org.json.JSONException;
import java.util.List;


public class Pembayaran extends Fragment View.OnClickListener {
    private MainActivity activ;
    private String namaPenumpang;
    private String tujuan;
    private String asalKeberangkatan;
    private String waktuKeberangkatan;
    private String posisiKursi;
    private double biaya;
    private int jumlahKursi;
    private Context cont;
    private PembayaranBinding binding;

    public static Pembayaran newInstance(MainActivity activ, Context cont)
        {
            Pembayaran pemb=new Pembayaran(activ,cont);
            return pemb;
        }


    public Pembayaran(MainActivity activ, Context cont)
        {
            this.activ = activ;
            this.cont = cont;
        }



    public void updateSeat(String namaPenumpang, String tujuan, String asalKeberangkatan, String waktuKeberangkatan, String posisiKursi, double biaya, int jumlahKursi, String posisiKursi)
        {
            this.namaPenumpang = namaPenumpang;
            this.tujuan=tujuan;
            this.asalKeberangkatan=asalkeberangkatan;
            this.waktuKeberangkatan=waktuKeberangkatan;
            this.posisiKursi=posisiKursi;
            this.biaya=biaya;
            this.jumlahKursi=jumlahKursi;
            this.posisiKursi=posisiKursi;

        }

    public void onClick(View vw)
        {
            if(vw == this.binding.btnCancel)
            {
                Bundle bn = new Bundle();
                bn.putIn("page", 1);
                this.getParentFragmentManager().setFragmentResult("changePage", bn);
            }
        }

    public void TiketUI(String nama)
        {
            Bundle bn = new Bundle();
            bn.putString("Nama Penumpang",this.nama);
            bn.putString("Tujuan",this.tujuan);
            bn.putString("Asal Keberangkatan",this.asalKeberangkatan);
            bn.putString("Waktu Keberangkatan,"this.waktuKeberangkatan);
            bn.putString("Posisi Kursi",this.posisiKursi);
            bn.putDouble("Harga Tiket",this.biaya);

        }
}
