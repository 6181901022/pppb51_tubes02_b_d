package com.example.travel;

import android.content.Context;
import android.util.Log;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.example.travel.databinding.FragmentSeatBinding;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class SeatPresenter extends AppCompatActivity {
    private Context context;
    private Gson gson;
    final String BASE_URL = "https://devel.loconode.com/pppb/v1/orders";
    private JSONObject json;
    private FragmentSeatBinding binding;
    BusKecil busKecil;
    BusBesar busBesar;

    public SeatPresenter(Context context) {
        this.context = context;
        this.busBesar=new BusBesar();
        this.busKecil= new BusKecil();

    }

    public void execute(String course_id, String seat) {

        Seat s=new Seat(course_id,seat);
        Spinner mySpinner = (Spinner) findViewById(R.id.sp_ukuran);
        String ukuran = mySpinner.getSelectedItem().toString();
        if (ukuran.equals("Besar")) {

            this.gson = new Gson();
            try {
                json = new JSONObject(gson.toJson(s));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if (ukuran.equals(("Kecil"))) {
            this.gson = new Gson();
            try {
                json = new JSONObject(gson.toJson(s));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        callVolley(json);
    }

    private void callVolley(JSONObject toJson) {
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, BASE_URL, toJson,
                response -> {
                    try {
                        process(response.toString());
                        Toast.makeText(context, "berhasil dipesan",
                                Toast.LENGTH_SHORT).show();
                        Log.d("OrderId", response.getString("order_id"));
                    } catch (JSONException e) {
                        Log.d("RESPONSE-ERROR", "error");
                        e.printStackTrace();
                    }


                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(context, "kursi sudah dipesan",
                        Toast.LENGTH_SHORT).show();
                Log.d("RESPONSE-ERROR", "error");
            }
        });
        requestQueue.add(jsonRequest);
    }
    private void process(String json) {
        ResultNewOrder respon= this.gson.fromJson(json,ResultNewOrder.class);
        List seat= respon.getSeat();
        String pesan = respon.getPesan();

    }


}
