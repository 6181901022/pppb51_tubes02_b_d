package com.example.travel;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.travel.databinding.FragmentSeatBinding;

public class BusKecil extends AppCompatActivity implements View.OnClickListener, View.OnTouchListener, GestureDetector.OnGestureListener, ScaleGestureDetector.OnScaleGestureListener{
    private Bitmap mBitmap;
    private FragmentSeatBinding binding;
    private Canvas mCanvas;
    private Paint kosong,isi,numSeat,numSeatIsi;
    private ImageView ivCanvas;
    private GestureDetector mDetector;
    private int [] busKecil;
    InitCanvas initCanvas;
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = FragmentSeatBinding .inflate(inflater, container, false);

        View view = this.binding.getRoot();
        this.initCanvas=new InitCanvas();
        initCanvas.bitMap();
        initCanvas.bg();
        initCanvas.pain();
        this.busKecil=new int[10];//inisiasi bus besar. jika 1 maka udah di klik dan 0 belum di klik
        for(int a=0;a<6;a++){
            busKecil[a]=0;
        }

        return view;

    }
    @Override
    protected void onResume() {
        super.onResume();
        initCanvas.gambarKecil();
    }
    @Override
    public boolean onDown(MotionEvent motionEvent) {
        float x=motionEvent.getX();
        float y=motionEvent.getY();
        if(x>=190 && x<= 260 && y>= 10 && y<=80 ){//seat 1
            if(busKecil[0]==0){
                mCanvas.drawRect(190,10,260,80,isi);
                mCanvas.drawText("1",225,45,numSeat);
                busKecil[0]=1;
            }else{
                mCanvas.drawRect(190,10,260,80,kosong);
                mCanvas.drawText("1",225,45,numSeat);
                busKecil[0]=1;
            }
        }
        if(x>=10 && x<=80 && y>=110 && y<=180){//seat 2
            if(busKecil[1]==1){
                mCanvas.drawRect(10,110,80,180,kosong);
                mCanvas.drawText("2",40,145,numSeat);
                busKecil[1]=0;
            }else{
                mCanvas.drawRect(10,110,80,180,isi);
                mCanvas.drawText("2",40,145,numSeat);
                busKecil[1]=1;
            }
        }
        if(x>=190 && x<=80 && y>=110 && y<= 180){//seat 3
            if(busKecil[2]==0){
                mCanvas.drawRect(190,110,260,180,isi);
                mCanvas.drawText("3",225,145,numSeat);
                busKecil[2]=1;
            }else{
                mCanvas.drawRect(190,110,260,180,kosong);
                mCanvas.drawText("3",225,145,numSeat);
                busKecil[2]=0;
            }
        }
        if(x>= 10 && x<= 80 && y>= 210 && y<= 280){//seat 4
            if(busKecil[3]==0){
                mCanvas.drawRect(10,210,80,280,isi);
                mCanvas.drawText("4",40,245,numSeat);
                busKecil[3]=1;
            }else{
                mCanvas.drawRect(10,210,80,280,kosong);
                mCanvas.drawText("4",40,245,numSeat);
                busKecil[3]=0;
            }
        }
        if(x>= 100&& x<=170 && y>= 210 && y<= 280){//seat 5
            if(busKecil[4]==0){
                mCanvas.drawRect(100,210,170,280,isi);
                mCanvas.drawText("5",135,245,numSeat);
                busKecil[4]=1;
            }else{
                mCanvas.drawRect(100,210,170,280,kosong);
                mCanvas.drawText("5",135,245,numSeat);
                busKecil[4]=0;
            }
        }
        if(x>=190 && x<=260 && y>= 210 && y<= 280){//seat 6
            if(busKecil[5]==0){
                mCanvas.drawRect(190,210,260,280,isi);
                mCanvas.drawText("6",225,245,numSeat);
                busKecil[5]=1;
            }else{
                mCanvas.drawRect(190,210,260,280,kosong);
                mCanvas.drawText("6",225,245,numSeat);
                busKecil[5]=0;
            }
        }
        this.binding.ivSeat.invalidate();
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public boolean onScale(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    @Override
    public boolean onScaleBegin(ScaleGestureDetector scaleGestureDetector) {
        return false;
    }

    @Override
    public void onScaleEnd(ScaleGestureDetector scaleGestureDetector) {

    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        return false;
    }
}