package com.example.travel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.example.travel.databinding.FragmentHomeBinding;

public class FragmentHome extends Fragment implements  View.OnClickListener{
    FragmentHomeBinding binding;

    @Override
    public View onCreateView(LayoutInflater inflater , ViewGroup container , Bundle savedInstanceState){
        this.binding = FragmentHomeBinding.inflate(inflater, container, false);

        View view = this.binding.getRoot();
        return view;

    }
    @Override
    public void onClick(View view) {
        if(view==this.binding.btnPesan){
            Bundle result = new Bundle();
            result.putInt("page", 3);
            this.getParentFragmentManager().setFragmentResult("changePage", result);
        }
    }
}
